﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObjects : MonoBehaviour
{

    private GameObject selectedGameObject;
    private GameObject previousGameObject;
    private Color defaultColor;


    private int index = 0;
    private int newIndex = 0;
    private bool isSelected = false;


    void Awake()
    {
        selectedGameObject = this.gameObject;
    }

    void Start()
    {
        defaultColor = gameObject.GetComponent<Renderer>().material.color;
        VisManager._instance.GetDefaultColor(defaultColor);
    }

    void OnMouseDown()
    {
        GameObject temp = this.gameObject;
        VisManager._instance.SelectedGameObject(true, selectedGameObject);
        VisManager.HighlightGameObject(true, selectedGameObject, Color.green);
        IsSelected = true;

    }

    void OnMouseUp()
    {
        IsSelected = false;
    }

    void OnMouseEnter()
    {
        selectedGameObject = this.gameObject;

        if (!IsSelected)
        {
            previousGameObject = gameObject;
            VisManager.HighlightGameObject(true, selectedGameObject, Color.blue);
        }
    }

    void OnMouseExit()
    {
        if (!IsSelected)
        {
            VisManager._instance.SelectedGameObject(false, selectedGameObject);
            VisManager.HighlightGameObject(false, previousGameObject, defaultColor);
        }
    }

    public GameObject SelectedGameObject { get { return selectedGameObject; } set { selectedGameObject = SelectedGameObject; } }
    public GameObject PreviousGameObject { get { return previousGameObject; } set { previousGameObject = PreviousGameObject; } }
    public bool IsSelected { get { return isSelected; } set { isSelected = IsSelected; } }
}
