﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeLook : MonoBehaviour {

    public float mouseSensitivity = 100.0f;
    public float clampAngle = 80.0f;
    
    private float rotY = 0.0f; // rotation around the up/y axis
    private float rotX = 0.0f; // rotation around the right/x axis
    private bool isCursorOn = false;
    public InteractableObjects[] interactableObjects;

    void Start()
    {
        interactableObjects = FindObjectsOfType(typeof(InteractableObjects)) as InteractableObjects[];
        Cursor.visible = false;
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;
    }

    void Update()
    {
        if(Input.GetButton("LShift"))
        {
            Cursor.visible = false;

            for (int i = 0; i < 0; i++)
            {
                Debug.Log("IO true");
                interactableObjects[i].enabled = false;
            }
        }
        else
        {
            Cursor.visible = true;
        }

        if (!Cursor.visible)
        {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = -Input.GetAxis("Mouse Y");

            rotY += mouseX * mouseSensitivity * Time.deltaTime;
            rotX += mouseY * mouseSensitivity * Time.deltaTime;

            rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

            Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
            transform.rotation = localRotation;
        }
    }
}
