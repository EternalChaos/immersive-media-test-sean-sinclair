﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VisManager : MonoBehaviour {

    public static VisManager _instance = null;
    public Text gameObjectText;
    public GameObject button;

    private static Color defaultColor;

    void Awake()
    {
        if (_instance == null)
            _instance = this;

        else if (_instance != this)
            Destroy(gameObject);
            DontDestroyOnLoad(gameObject);

    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {

        }
    }

    public void GetDefaultColor(Color C)
    {
        defaultColor = C;
    }

    public void SelectedGameObject(bool B, GameObject G)
    {
        if (B)
        {
            gameObjectText.text = G.name;
        }
        else
        {
            gameObjectText.text = "";
        }
    }

    public static CustomGameObject HighlightGameObject(bool B, GameObject G)
    {
        return HighlightGameObject(B, CustomGameObject.ReturnedGameObject(), CustomColor.ReturnedColor());
    }

    public static CustomGameObject HighlightGameObject(bool B, GameObject G, Color C)
    {
        if (B)
        {
            G.GetComponent<Renderer>().material.color = C;
        }
        else
        {
            //G.GetComponent<Renderer>().material.color = Color.grey;
            G.GetComponent<Renderer>().material.color = defaultColor;
        }

        return null;
    }

    public void ExitApplication()
    {
        Application.Quit();
    }
}
